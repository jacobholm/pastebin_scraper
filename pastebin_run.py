import pastebin_fetch_latest
import pastebin_processor
import pastebin_notify
import json
import MySQLdb
import datetime
import time

with open('.config') as json_data_file:
	config = json.load(json_data_file)

db = MySQLdb.connect(config["mysql"]["host"], config["mysql"]["user"], config["mysql"]["password"], config["mysql"]["database"], autocommit=True)
done = False
processSleep = config["other"]["processSleep"]

while (done == False):
	startTime = time.time()
	print("\nTimestamp: {:%Y-%b-%d %H:%M:%S}".format(datetime.datetime.now()) + "\n")

	print ("-- Fetch pastes --\n")
	pastebin_fetch_latest.run(db, config)

	print ("\n-- Process pastes --\n")
	pastebin_processor.run(db, config)

	print ("\n-- Send notifications --\n")
	pastebin_notify.run(db, config)

	elapsedTime = time.time() - startTime
	print ("Elapsed time: " + str(elapsedTime))

	if elapsedTime >= processSleep:
		print ("Processing time exceeded sleep config value, running again immediately")
	else:
		sleepDiff = processSleep - elapsedTime
		print ("\nSleeping for " + str(sleepDiff) + " seconds...")
		time.sleep(sleepDiff)

	print ("--------------------------------------")

db.close()
