-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.7-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pastebin
CREATE DATABASE IF NOT EXISTS `pastebin` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pastebin`;

-- Dumping structure for procedure pastebin.add_searchterm
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_searchterm`(
	IN `in_searchterm` VARCHAR(100),
	IN `in_subscriber_name` VARCHAR(50)







,
	IN `in_is_regex` INT



)
    MODIFIES SQL DATA
    COMMENT 'Adds a new keyword to the keywords table, and creates a relationship to the specified subscriber. If the keyword already exists, it simply creates a relationship.'
BEGIN
	
	IF in_subscriber_name IS NULL THEN
		SET in_subscriber_name = 'unassigned';
	END IF;
	
	-- Insert keyword if it does not already exist
	INSERT INTO searchterms (id, searchterm, is_regex)
	SELECT * FROM (SELECT (SELECT MAX(id)+1 FROM searchterms), in_searchterm, in_is_regex) AS tmp
	WHERE NOT EXISTS (
	    SELECT id, searchterm, is_regex FROM searchterms WHERE searchterm = in_searchterm
	) LIMIT 1;
	
	-- Create a relationship between subscriber and keyword
	INSERT INTO subscribers_searchterms (subscribers_name_fk, searchterms_id_fk)
	SELECT * FROM (SELECT in_subscriber_name, (SELECT id FROM searchterms where searchterm = in_searchterm)) AS tmp
	WHERE NOT EXISTS (
	    SELECT * FROM subscribers_searchterms WHERE subscribers_name_fk = in_subscriber_name AND searchterms_id_fk = (SELECT id FROM searchterms where searchterm = in_searchterm)
	) LIMIT 1;
	
END//
DELIMITER ;

-- Dumping structure for procedure pastebin.add_subscriber
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_subscriber`(
	IN `in_name` VARCHAR(50),
	IN `in_email` VARCHAR(50),
	IN `in_receive_all` VARCHAR(50)
)
BEGIN
	-- Insert subscriber if he/she does not already exist
	INSERT INTO subscribers (name, email, receive_all)
	SELECT * FROM (SELECT in_name, in_email, in_receive_all) AS tmp
	WHERE NOT EXISTS (
	    SELECT name FROM subscribers WHERE name = in_name
	) LIMIT 1;
END//
DELIMITER ;

-- Dumping structure for procedure pastebin.cleanup_queue
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `cleanup_queue`(
	IN `in_nrOfPastes` INT
)
BEGIN
	SET @cutoff := (SELECT date FROM (SELECT date FROM queue ORDER BY date DESC LIMIT in_nrOfPastes) AS T ORDER BY date ASC LIMIT 1);
	
	DELETE FROM queue
	WHERE date < @cutoff AND processed = 1;
END//
DELIMITER ;

-- Dumping structure for procedure pastebin.get_subscribers_hits
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_subscribers_hits`()
BEGIN
	SELECT subscribers_name_fk, results.`key` FROM subscribers_results LEFT JOIN (results) ON (results_id_fk = results.id) WHERE subscribers_results.notified = 0;
END//
DELIMITER ;

-- Dumping structure for procedure pastebin.get_subscriber_hit_searchterms
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_subscriber_hit_searchterms`(
	IN `in_paste_key` VARCHAR(50),
	IN `in_subscriber_name` VARCHAR(50)
)
    COMMENT 'Get the searchterms for a specific hit'
BEGIN
	SELECT searchterm FROM searchterms RIGHT JOIN (
	SELECT results_searchterms.searchterms_id_fk
	FROM results_searchterms LEFT JOIN (subscribers_searchterms, results)
	ON (results_id_fk = results.id AND results_searchterms.searchterms_id_fk = subscribers_searchterms.searchterms_id_fk)
	WHERE results.`key` = in_paste_key AND subscribers_searchterms.subscribers_name_fk = in_subscriber_name
	) AS tmp ON (id = searchterms_id_fk);
END//
DELIMITER ;

-- Dumping structure for table pastebin.queue
CREATE TABLE IF NOT EXISTS `queue` (
  `scrape_url` varchar(100) DEFAULT NULL,
  `full_url` varchar(100) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `syntax` varchar(100) DEFAULT NULL,
  `user` varchar(100) DEFAULT NULL,
  `processed` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pastebin.results
CREATE TABLE IF NOT EXISTS `results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `key` varchar(50) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `expire` int(11) DEFAULT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `syntax` varchar(50) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pastebin.results_searchterms
CREATE TABLE IF NOT EXISTS `results_searchterms` (
  `results_id_fk` int(11) NOT NULL,
  `searchterms_id_fk` int(11) NOT NULL,
  UNIQUE KEY `results_id_fk_searchterms_id_fk` (`results_id_fk`,`searchterms_id_fk`),
  KEY `FK_results_searchterms_searchterms` (`searchterms_id_fk`),
  CONSTRAINT `FK_results_searchterms_results` FOREIGN KEY (`results_id_fk`) REFERENCES `results` (`id`),
  CONSTRAINT `FK_results_searchterms_searchterms` FOREIGN KEY (`searchterms_id_fk`) REFERENCES `searchterms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pastebin.searchterms
CREATE TABLE IF NOT EXISTS `searchterms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `searchterm` varchar(100) NOT NULL,
  `is_regex` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `searchterm` (`searchterm`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pastebin.subscribers
CREATE TABLE IF NOT EXISTS `subscribers` (
  `name` varchar(50) NOT NULL DEFAULT 'unknown',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pastebin.subscribers_results
CREATE TABLE IF NOT EXISTS `subscribers_results` (
  `subscribers_name_fk` varchar(50) NOT NULL,
  `results_id_fk` int(11) NOT NULL,
  `notified` int(11) NOT NULL DEFAULT 0,
  `verified` int(11) NOT NULL DEFAULT 0,
  UNIQUE KEY `subscribers_name_fk_results_id_fk` (`subscribers_name_fk`,`results_id_fk`),
  KEY `FK_subscribers_results_subscribers` (`subscribers_name_fk`),
  KEY `FK_subscribers_results_results` (`results_id_fk`),
  CONSTRAINT `FK_subscribers_results_results` FOREIGN KEY (`results_id_fk`) REFERENCES `results` (`id`),
  CONSTRAINT `FK_subscribers_results_subscribers` FOREIGN KEY (`subscribers_name_fk`) REFERENCES `subscribers` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Mapping between results and subscribers, based on which searchterms the subscriber subscribes to.';

-- Data exporting was unselected.
-- Dumping structure for table pastebin.subscribers_searchterms
CREATE TABLE IF NOT EXISTS `subscribers_searchterms` (
  `subscribers_name_fk` varchar(50) NOT NULL,
  `searchterms_id_fk` int(11) NOT NULL,
  UNIQUE KEY `subscribers_name_fk_searchterms_searchterm_fk` (`subscribers_name_fk`,`searchterms_id_fk`),
  KEY `FK_subscribers_searchterms_searchterms` (`searchterms_id_fk`),
  CONSTRAINT `FK_subscribers_searchterms_searchterms` FOREIGN KEY (`searchterms_id_fk`) REFERENCES `searchterms` (`id`),
  CONSTRAINT `FK_subscribers_searchterms_subscribers` FOREIGN KEY (`subscribers_name_fk`) REFERENCES `subscribers` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pastebin.subscribers_settings
CREATE TABLE IF NOT EXISTS `subscribers_settings` (
  `subscriber_name_fk` varchar(50) NOT NULL,
  `uid` varchar(50) DEFAULT NULL,
  `group` int(11) NOT NULL DEFAULT 0,
  `notification_realtime` int(1) NOT NULL DEFAULT 1,
  `notification_day` datetime DEFAULT NULL COMMENT 'Day and time of the week to receive notifications',
  `receive_all_notifications` int(11) NOT NULL DEFAULT 0,
  UNIQUE KEY `subscriber_name_fk` (`subscriber_name_fk`),
  CONSTRAINT `FK_subscribers_settings_subscribers` FOREIGN KEY (`subscriber_name_fk`) REFERENCES `subscribers` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table pastebin.subscriber_groups
CREATE TABLE IF NOT EXISTS `subscriber_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
