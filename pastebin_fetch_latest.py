import requests
import urllib3
import json
import time

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def cleanupQueue(db, nrOfPastes):
  cursor = db.cursor()

  sql = "call cleanup_queue(" + str(nrOfPastes) + ")"

  try:
    cursor.execute(sql)
    print ("Cleaned up queue...")
  except Exception as e:
    print ("Error: unable to execute cleanup procedure: " + str(e))
    db.rollback()
    return -1

  return 0

def fetchPasteQueueKeys(db):
  pasteKeys = []
  cursor = db.cursor()

  # Fetch keywords
  sql = "SELECT `key` FROM queue"

  try:
    cursor.execute(sql)
    results = cursor.fetchall()

    for row in results:
      pasteKeys.append(row[0])

    return pasteKeys

  except Exception as e:
    print ("Error: unable to fetch data: " + str(e))
    return []

def fetchLatestPastes(pasteQueueKeys, api, retries, nrOfPastes, timeout):
  # Get the latest x posts from pastebin
  response = ""
  done = False
  retryCounter = 0

  while not done:
    try:
      response = requests.get(api + '?limit=' + str(nrOfPastes), verify=False, timeout=timeout)
      done = True
    except Exception as ex:
      if retryCounter == retries:
        done = True
      else:
        print ("Request timeout, retry...")
        time.sleep(5)
        retryCounter += 1

  if response == "":
    print ("ERROR: Could not fetch the latest pastes")
    return []

  try:
    jsonData = json.loads(response.content.decode("utf-8", "backslashreplace"))
  except Exception as ex:
    print ("Could not load response content as JSON data: " + str(ex))
    print (response.content)
    return []

  # Skip the pastes that are already in the queue
  newPastes = []
  for paste in jsonData:
    if paste["key"] not in pasteQueueKeys:
      newPastes.append(paste)

  print ("Fetched " + str(len(newPastes)) + " new pastes...")

  return newPastes

def storePastesInQueue(db, pastes):
  print ("Storing the pastes in the database...")

  # Prepare a cursor object using cursor() method
  cursor = db.cursor()

  # Store each paste
  for paste in pastes:
    truncatedTitle = ((paste["title"][:195] + '...') if len(paste["title"]) > 195 else paste["title"]).encode("utf-8")

    # Python does not handle multiline very well, therefore we must wrap all parameters in str()
    # TODO: If title is longer than 200 chars, truncate it...
    sql = "INSERT INTO queue (scrape_url, full_url, date, `key`, size, expire, title, syntax, user, processed) VALUES (" + \
          "'" + str(paste["scrape_url"]) + "', " + \
          "'" + str(paste["full_url"]) + "', " + \
          str(paste["date"]) + ", " + \
          "'" + str(paste["key"]) + "', " + \
          str(paste["size"]) + ", " + \
          str(paste["expire"]) + ", " + \
          "'" + str(truncatedTitle).replace("'", "\"") + "', " + \
          "'" + str(paste["syntax"]) + "', " + \
          "'" + str(paste["user"]) + "', " + \
          str(0) + \
          ")"
    
    try:
      cursor.execute(sql)
      
    except Exception as ex:
      print ("Error: unable to insert data: " + str(ex))
      print ("Failing SQL query: " + sql)
      db.rollback()

def run(db, config):
  pasteQueueKeys = fetchPasteQueueKeys(db)
  latestPastes = fetchLatestPastes(pasteQueueKeys, config["fetch"]["api"], config["other"]["requestRetries"], config["fetch"]["nrOfPastes"], config["other"]["requestTimeout"])  # TODO: Check that latestPastes != -1, if it is -1 something went wrong
  
  if len(latestPastes) > 0:
    storePastesInQueue(db, latestPastes)

  cleanupQueue(db, config["fetch"]["nrOfPastes"])