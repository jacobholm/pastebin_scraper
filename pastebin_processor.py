import requests
import urllib3
import time
import re

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def fetchPasteQueue(db):
  pasteQueue = []

  try:
    cursor = db.cursor()
    sql = "SELECT * FROM queue WHERE processed = 0"

    cursor.execute(sql)
    results = cursor.fetchall()

    for paste in results:
      pasteQueue.append({"scrape_url": paste[0], "full_url": paste[1], "date": paste[2], "key": paste[3], "size": paste[4], "expire": paste[5], "title": paste[6], "syntax": paste[7], "user": paste[8], "processed": paste[9], "content": ""})

  except Exception as e:
    print("Failed to fetch paste queue: " + str(e))
  finally:
    return pasteQueue

def fetchSearchterms(db):
  parsedResults = []

  try:
    cursor = db.cursor()
    sql = "SELECT * FROM searchterms"
    cursor.execute(sql)
    results = cursor.fetchall()
  except Exception as e:
    print ("Failed to fetch searchterms: " + str(e))
  finally:
    for searchterm in results:
      parsedResults.append({"searchterm_id": searchterm[0], "searchterm": searchterm[1], "is_regex": searchterm[2]})

    return parsedResults

def searchPaste(paste, decodedContent, searchterms):
  results = []

  try:
    for searchterm in searchterms:
      found = False
      term = searchterm["searchterm"]

      if searchterm["is_regex"] == 1:
        match = re.search(r"" + term, decodedContent)
        
        if match:
          found = True
      elif decodedContent.find(term.lower()) > -1:
        found = True
      
      if found:
        print ("Found searchterm: " + term)
        results.append(searchterm)

  except re.error as ex:
    print("Regex error: " + str(ex))
  except Exception as e:
    print("Failed to search: " + str(e))
  finally:
    return results

def processPastes(searchterms, pasteQueue, retries, timeout, db):
  pasteHits = 0
  counter = 1;

  for paste in pasteQueue:
    try:
      response = ""
      retryCounter = 0
      done = False

      print ("Processing " + str(counter) + "/" + str(len(pasteQueue)) + ": " + paste["key"])

      while not done:
        try:
          response = requests.get(paste["scrape_url"], verify=False, timeout=timeout)
          done = True

          if retryCounter > 0:
            print ("Retry was successful")
        except Exception as e:
          if retryCounter == retries:
            done = True
            print ("Max number of retries hit, skipping paste")
          else:
            print ("Request timeout, retry...")
            retryCounter += 1
            time.sleep(5)

      if response == "":
        print ("ERROR: Request timed out (" + str(retryCounter) + " retries)")
        continue

      decodedContent = response.content.decode("utf-8", "backslashreplace").lower()

      searchResult = searchPaste(paste, decodedContent, searchterms)

      if len(searchResult) > 0:
        paste["content"] = decodedContent
        pasteHits += 1

        # Store search results and set the pastes as processed in the database
        if (storeSearchResultOnDisk(paste) == -1) or (storeSearchResultInDb(db, paste, searchResult) == -1):
          continue

      setPasteProcessed(db, paste)

    except Exception as e:
      print("Failed to process paste: " + str(e))
    finally:
      counter += 1

  return pasteHits

def storeSearchResultOnDisk(paste):
  try:
    fileName = paste["key"] + ".txt"
    f = open("results/" + fileName, 'w', encoding="utf-8")
    f.write(paste["content"])
    f.close()

  except Exception as e:
    print ("Failed to store paste on disk: " + fileName)
    print (str(e))
    return -1

  return 0

# TODO: Vulnerable to SQL injection!?
def storeSearchResultInDb(db, paste, searchResult):
  cursor = db.cursor()

  # -------------------- Store result in results table --------------------

  # Python does not handle multiline very well, therefore we must wrap all parameters in str()
  sql = "INSERT INTO results (id, date, `key`, size, expire, title, syntax, user) VALUES (" + \
        str(0) + "," + \
        str(paste["date"]) + "," + \
        "'" + str(paste["key"]) + "'," + \
        str(paste["size"]) + "," + \
        str(paste["expire"]) + "," + \
        "'" + str(paste["title"]).replace("'", "\"") + "'," + \
        "'" + str(paste["syntax"]) + "'," + \
        "'" + str(paste["user"]) + "'" + \
        ")"

  try:
    cursor.execute(sql)

  except Exception as e:
    print ("Error: unable to insert data: " + str(e))
    print ("Failing SQL query: " + sql)
    db.rollback()

    return -1

  # -------------------- Create relationships between result - searchterms, and result - subscribers --------------------

  # Fetch the result id
  resultId = -1
  sql = "SELECT id FROM results WHERE `key` = '" + paste["key"] + "'"

  try:
    cursor.execute(sql)
    resultId = cursor.fetchone()

  except Exception as e:
    print ("Error: unable to select data: " + str(e))
    print ("Failing SQL query: " + sql)
    db.rollback()

    return -1

  if resultId == -1:
    print ("resultId == -1")
    return -1

  processedSubscribers = []

  for searchterm in searchResult:
    try:
      # Create relaltionships in the results_searchterms table
      sql = "INSERT INTO results_searchterms (results_id_fk, searchterms_id_fk) VALUES (" + str(resultId[0]) + ", " + str(searchterm["searchterm_id"]) + ")"
      cursor.execute(sql)
      
      # Create relaltionships in the subscribers_results table
      sql = "SELECT subscribers_name_fk FROM subscribers_searchterms WHERE searchterms_id_fk = '" + str(searchterm["searchterm_id"]) + "'"
      cursor.execute(sql)
      subscribers = cursor.fetchall()

      for subscriber in subscribers:
        if subscriber[0] not in processedSubscribers:
          sql = "INSERT INTO subscribers_results (subscribers_name_fk, results_id_fk, notified, verified) VALUES ('" + \
          str(subscriber[0]) + "'," + \
          "'" + str(resultId[0]) + "'," + \
          str(0) + "," + \
          str(0) + ")"

          processedSubscribers.append(subscriber[0])

          cursor.execute(sql)

    except Exception as ex:
      print ("Error: " + str(ex))
      print ("Failing SQL query: " + sql)
      db.rollback()

      return -1

  return 0

def setPasteProcessed(db, paste):
  cursor = db.cursor()

  # TODO: Vulnerable to SQL injection!
  # Python does not handle multiline very well, therefore we must wrap all parameters in str()
  sql = "UPDATE queue SET processed = 1 WHERE `key` = '" + paste["key"] + "'"
  
  try:
    cursor.execute(sql)

  except Exception as ex:
    print ("Error: unable to insert data: " + str(ex))
    print ("Failing SQL query: " + sql)
    db.rollback()

    return -1

  return 0

# ---------------------------------------------------------------

def run(db, config):
  searchterms = fetchSearchterms(db)
  pasteQueue = fetchPasteQueue(db)

  if len(pasteQueue) > 0:
    pasteHits = processPastes(searchterms, pasteQueue, config["other"]["requestRetries"], config["other"]["requestTimeout"], db)
  else:
    print ("No new pastes...")
