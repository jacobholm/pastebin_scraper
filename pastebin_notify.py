import requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def getSubscribersHits(db):

	subscribersHits = []

	cursor = db.cursor()
	sql = "CALL get_subscribers_hits()"

	try:
		cursor.execute(sql)
		results = cursor.fetchall()
		cursor.close()

		for row in results:
			# Fetch all searchterms related to this paste, for this particular subscriber
			tmpHit = {"pasteKey": row[1], "searchterms": [], "subscriber": row[0]}

			sql = "CALL get_subscriber_hit_searchterms('" + tmpHit["pasteKey"] + "', '" + tmpHit["subscriber"] + "')"

			cursor = db.cursor()
			cursor.execute(sql)
			subscriberSearchterms = cursor.fetchall()
			cursor.close()

			for term in subscriberSearchterms:
				tmpHit["searchterms"].append(term[0])

			subscribersHits.append(tmpHit)

		return subscribersHits

	except Exception as e:
		print ("Error: unable to fetch data")
		print (str(e))
		return []

def sendNotification(subscriberHit, config):
	try:

		body = str(len(subscriberHit["searchterms"])) + " searchterm(s) hit: "
		starred = "false"
		uids = ["uid" + subscriberHit["subscriber"]]

		for searchterm in subscriberHit["searchterms"]:
			body += "'" + searchterm + "', "
		body = body[:-2]

		# POST request
		data = '{"notification": {"body": "' + body + \
				'", "title": "' + config["notifications"]["title"] + \
				'", "target_url": "' + config["notifications"]["targetUrl"] + "/" + subscriberHit["pasteKey"] + \
				'", "icon_url": "' + config["notifications"]["iconUrl"] + \
				'", "image_url": "' + config["notifications"]["imageUrl"] + \
				'", "ttl": ' + str(config["notifications"]["timeToLive"]) + \
				', "require_interaction": ' + config["notifications"]["requireInteraction"] + \
				', "starred": ' + starred + \
				'}, "uids": ' + str(uids).replace("'", "\"") + '}'

		response = requests.post(config["notifications"]["api"], data = data, headers = config["notifications"]["headers"])

		if response.status_code == 201:
			print ("Notification sent successfully to " + subscriberHit["subscriber"])
		else:
			print ("Error: Failed to send notification to " + subscriberHit["subscriber"] + "\nStatus code: " + str(response.status_code) + "\nResponse body: " + str(response.content))
			return -1

	except Exception as e:
		print ("Error: " + str(e))
		print (subscriberHit)
		return -1
	
	return 0

def setNotificationSent(db, hit):

	cursor = db.cursor()
	sql = "UPDATE subscribers_results SET notified = 1 WHERE subscribers_name_fk = '" + hit["subscriber"] + "' AND results_id_fk = (SELECT id FROM results WHERE `key` = '" + hit["pasteKey"] + "')"

	try:
		cursor.execute(sql)
	except Exception as e:
		print ("Error: Unable to update result: " + str(e))
		db.rollback()
		return -1

	return 0

def run(db, config):
	hits = getSubscribersHits(db)

	if len(hits) > 0:
		for hit in hits:
			if sendNotification(hit, config) == 0:
				setNotificationSent(db, hit)
	else:
		print ("No new hits")
